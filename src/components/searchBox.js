import React from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import { MdKeyboardBackspace } from "react-icons/md";
import { FaSitemap } from "react-icons/fa";

function SearchBox(){
    return (
        <div className="mb-3 searchBox">
            <InputGroup>
                <InputGroup.Prepend>
                    <InputGroup.Text><MdKeyboardBackspace /></InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl placeholder='Search for products' />
                <InputGroup.Append>
                    <InputGroup.Text><FaSitemap /></InputGroup.Text>
                </InputGroup.Append>
            </InputGroup>
        </div>
    )
}

export default SearchBox;