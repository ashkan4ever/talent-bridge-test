import React from 'react';

function PieDetail(props) {
        var data = props.data
        return (
            <div className='staticsContainer'>
                {
                    data.map(info => {
                        return (
                            <div key={info.index} className='staticContainer'>
                                <div >
                                    {info.icon}
                                    <span className='staticName'>{info.name}</span>
                                    <span className='staticPercent'>{info.percent + '%'}</span>
                                </div>
                                <div className='staticBar' style={{ width: info.percent+'%', backgroundColor: info.percent > 63 ? '#84d862' : '#fdc600'}}></div>
                            </div>
                        )
                    })
                }
            </div>
        )
}

export default PieDetail;