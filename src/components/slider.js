import React from 'react';
import Carousel from 'react-bootstrap/Carousel'

function Slider() {
    return (
        <div className='smallContainer'>
        <Carousel wrap={false} direction='ltr' slide={false} touch={true} interval={null}>
            <Carousel.Item>
                <div className='sliderItemContainer'>
                    <img alt='iphonexs' className='sliderItemImg' src='https://www.searchpng.com/wp-content/uploads/2018/12/Apple-iPhone-XS-PNG-Image-715x715.png'/>
                    <img alt='flag' className='slideFlag' src='/assets/img/flag.png' />
                </div>
            </Carousel.Item>
            <Carousel.Item>
                <div className='sliderItemContainer'>
                    <img alt='iphonexs' className='sliderItemImg' src='https://azfon.ae/image/cache/catalog/product/1all/phone/apple/xss/Gold/frn-1200x1200.png'/>
                </div>
            </Carousel.Item>
        </Carousel>
        </div>
    )
}

export default Slider;