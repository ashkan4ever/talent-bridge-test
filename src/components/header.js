import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { MdShoppingCart } from "react-icons/md";
import { FaSearch } from "react-icons/fa";

export class Header extends Component {
    render() {
        return (
            <Navbar expand="lg">
                <Navbar.Brand href="#home">
                    <img alt='logo' className='logo' src='/assets/img/logo.jpg' />
                </Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto"></Nav>
                    <FaSearch className='menuItem selectedMenuItem searchMenuItem'/>
                    <div className='menuItem shopMenuItem'>
                        <MdShoppingCart/>
                        <span className="notifNumber">2</span>
                    </div>
                    <div class="menuItem barsContainer">
                        <div class="bar bar1"></div>
                        <div class="bar bar2"></div>
                        <div class="bar bar3"></div>
                    </div>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default Header;