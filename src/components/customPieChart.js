import React, { Component } from 'react';
import { FaUsers, FaLightbulb, FaStar, FaXbox } from "react-icons/fa";

export class CustomPieChart extends Component {
    percentToDegree = (percent, dataLength) => {
        return (percent * Math.PI) / (dataLength * 50)
    }

    handleColor = (percent) => {
        return percent > 63 ? '#84d862' : '#fdc600'
    }

    calAve = (data) => {
        let sum = 0;
        data.forEach((info) => {
            sum += info.percent
        });
        return sum / data.length
    }

    componentDidMount() {
        var data = this.props.data
        var canvas = document.getElementById('chartCanvas');
        var context = canvas.getContext('2d');
        var y = canvas.height / 2;
        var x = canvas.width / 2;
        var radius = x;
        var counterClockwise = false;

        //Draw outer disk
        data.forEach( info => {
            var startAngle = info.index  * Math.PI / 2;
            var endAngle = (this.percentToDegree(info.percent, data.length)) + startAngle;
            context.beginPath();
            context.moveTo(x, y);
            context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
            context.lineTo(x, y);
            context.strokeStyle = this.handleColor(info.percent);
            context.fillStyle = this.handleColor(info.percent);
            context.fill();
            context.stroke();
        });

        // draw horizontal and vertical diameters of circle
        context.beginPath();
        context.moveTo(x, 0);
        context.lineTo(x, canvas.height);
        context.moveTo(0, y);
        context.lineTo(canvas.width, y);
        context.lineWidth = 3;
        context.strokeStyle = '#fff';
        context.stroke();

        //Draw inner disk
        context.beginPath();
        context.arc(x, y, radius/1.5, 0* Math.PI, 2*Math.PI, counterClockwise);
        context.strokeStyle = '#fff';
        context.fillStyle = this.calAve(data) > 63 ? '#84d862' : '#fdc600'
        context.fill()
        context.stroke();

        //Write text in the inner disk
        context.font = "900 30px Arial";
        context.fillStyle = "#114767"
        context.fillText(Math.ceil(this.calAve(data)) + '%', x- radius/2.5, y+10);
        context.font = "900 15px Arial";
        context.fillText('overall', x- radius/3, y+25);
    }
    render() {
        return (
            <div className='chartContainer'>
                <canvas id="chartCanvas" width="157" height="157"></canvas>
                <FaStar className='chartIcon star'/>
                <FaUsers className='chartIcon users'/>
                <FaXbox className='chartIcon fax'/>
                <FaLightbulb className='chartIcon light'/>
            </div>
        )
    }
}

export default CustomPieChart;