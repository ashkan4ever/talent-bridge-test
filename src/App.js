import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { FaAngleRight } from "react-icons/fa";
import Header from './components/header.js';
import SearchBox from './components/searchBox.js';
import Slider from './components/slider.js';
import CustomPieChart from './components/customPieChart.js';
import PieDetail from './components/pieDetail.js';
import { FaUsers, FaLightbulb, FaStar, FaXbox } from "react-icons/fa";

export class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          percent: 74,
          icon: <FaXbox />,
          name: 'Environment',
          index: 2
        },
        {
          percent: 59,
          icon: <FaUsers />,
          name: 'Social impact',
          index: 3
        },
        {
          percent: 70,
          icon: <FaStar />,
          name: 'Quality',
          index: 0
        },
        {
          percent: 71,
          icon: <FaLightbulb />,
          name: 'Innovation',
          index: 1
        }
      ]
    }
  }

  render() {
    return (      
      <div className='mainContainer'>
        <Header />
        <Row>
          <Col>
            <Row>
              <Col>
                <SearchBox />
              </Col>
            </Row>
            <Row>
              <Col>
                <h2 className='productName'>IPHONE XS</h2>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className='smallContainer'>
                    <div className='halfDiv'>
                      <CustomPieChart data={this.state.data} />
                    </div>
                    <div className='halfDiv'>
                      <PieDetail data={this.state.data} />
                    </div>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <a href='http://talentbridge.eu.com'><h5 className='moreInfo'>LEARN MORE <FaAngleRight /></h5></a>
              </Col>
            </Row>
            <Row>
              <Col>
                <Slider />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
