run local:

In the project directory, first run:

### `npm install`

and ther, run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

run docker:

In the project directory, first make an image(you can use your own optons):

### `docker build -t <username>/docker-react-sample .`

then, get list of your images:

### `docker images`

from the information run:

### `docker run -i -t <name>:<tag> /bin/bash`

